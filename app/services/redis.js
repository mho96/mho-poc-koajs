const redis = require('redis')
const client = redis.createClient();
const { promisify } = require('util')

class RedisService {
    constructor(redisClient) {
        this.client = redisClient;
    }

    async hget(set, key){
        console.log('ICI')
        const asyncHget = promisify(this.client.hget).bind(this.client)
        return await asyncHget(set, key);
    }

    async hgetall(set){
        const asyncHgetall = promisify(this.client.hgetall).bind(this.client)
        return await asyncHgetall(set);
    }

    async hset(set, key, value){
        const asyncHset = promisify(this.client.hset).bind(this.client)
        return await asyncHset(set, key, value);
    }

    async hdel(set, key){
        const asyncHdel = promisify(this.client.hdel).bind(this.client)
        return await asyncHdel(set, key);
    }
}

module.exports = new RedisService(client)