'use strict';

const Router = require('koa-router');
//const miscController = require('./controllers/misc');
const contentController = require('./controllers/content');


const router = new Router();
//router.get('/', miscController.getApiInfo);
//router.get('/spec', miscController.getSwaggerSpec);

router.get('/contents/:id', contentController.getOne);
router.get('/contents', contentController.getMany);
router.post('/contents', contentController.create);
router.put('/contents/:id', contentController.replace);
router.patch('/contents/:id', contentController.update);
router.del('/contents/:id', contentController.remove);

module.exports = router;
