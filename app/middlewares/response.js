const responseMiddleware = () => {
    return async function responseHandler(ctx, next) {
        const body = await next();
        ctx.body = body;
    };
};

module.exports = responseMiddleware;