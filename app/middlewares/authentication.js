const compose = require('koa-compose');
const jwt = require('koa-jwt');
// a ne pas reproduire à la maison lol
const config = require('./../config'); 

const AuthResponseMiddleare = async (ctx, next) => {
    return next().catch((err) => {
        if (401 == err.status) {
            ctx.status = 401;
            ctx.body = 'Protected resource, use Authorization header to get access\n';
        } else {
            throw err;
        }
    });
}

const authenticationMiddleware = () => {
    const { secret } = config;
    return compose([
        AuthResponseMiddleare,
        jwt({ secret: secret })
    ]);
};

module.exports = authenticationMiddleware;