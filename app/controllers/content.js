'use strict';

const contentService = require('./../services/content');
const errs = require('./../constants/error');
const uniqid = require('uniqid')

const getOne = async (ctx) => {
    try {
        const result = await contentService.getOne(ctx.params.id);
        if (result === null) {
            throw new Error(errs.UNKNOWN_RESOURCE);
        }
        return { id: ctx.params.id, ...JSON.parse(result)};
    } catch (err) {
        throw new Error(errs.INTERNAL_ERROR);
    }
}

const getMany = async (ctx) => {
    try {
        const contents = await contentService.getMany();
        if (contents === null) {
            throw new Error(errs.UNKNOWN_RESOURCE);
        }
        return Object.keys(contents).map(val => {
            return {
                id: val,
                ...JSON.parse(contents[val])
            }
        });
    } catch (err) {
        throw new Error(errs.INTERNAL_ERROR);
    }
}

const create = async (ctx) => {
    try {
        const id = uniqid();
        const fields = {
            title: ctx.request.body.title, 
            body: ctx.request.body.body
        };
        const payload = {
            ...fields,
            created: Math.floor(Date.now()/1000)
        };

        const result = await contentService.create(id, JSON.stringify(payload));
        if (!result) {
            throw new Error(errs.UNKNOWN_RESOURCE);
        }
        return await getOne({ params: { id } });
    } catch (err) {
        throw new Error(errs.INTERNAL_ERROR);
    }
}

const replace = async (ctx) => {
    try {
        const fields = {
            title: ctx.request.body.title, 
            body: ctx.request.body.body
        };
        const created = Math.floor(Date.now()/1000);
        const payload = { ...fields, created: created };
        const result = await contentService.create(
            ctx.params.id,
            JSON.stringify(payload)
        );

        if (result === null) {
            throw new Error(errs.UNKNOWN_RESOURCE);
        }
        return await getOne(ctx);
    } catch (err) {
        throw new Error(errs.INTERNAL_ERROR);
    }
}

const update = async (ctx) => {
    try {
        const { id, ...existing } = await getOne(ctx);
        const existingKeys = Object.keys(existing);
        const fields = Object.keys(ctx.request.body).filter(
            key => !['id', 'created'].includes(key) && existingKeys.includes(key)
        ).reduce((acc, curr) => {
            return { ...acc, [curr]: ctx.request.body[curr] }
        }, {});
    
        const merge = { ...existing, ...fields };
        const created = Math.floor(Date.now()/1000);
        const payload = { ...merge, created: created };

        await contentService.create(
            id,
            JSON.stringify(payload)
        );
        return await getOne(ctx);
    } catch (err) {
        throw new Error(errs.INTERNAL_ERROR);
    }
}

const remove = async (ctx) => {
    try {
        const result = await contentService.delete(ctx.params.id);
        if (!result) {
            throw new Error(errs.UNKNOWN_RESOURCE);
        }
        const message = result 
        ? `content ${ctx.params.id} has been deleted`
        : `content ${ctx.params.id} has not been deleted`
        return { message };
    } catch (err) {
        throw new Error(errs.INTERNAL_ERROR);
    }
}

module.exports = { getOne, getMany, create, replace, update, remove }